# Cassiopee: suffix search with errors

Cassiopee is a library to create a compressed suffix tree to search for a string
in a sequence.

It supports substitution and inserts or deletions.

Graph can be flat or compressed. Compressed form uses less memory but is less performant.

See tests or cli for usage examples.

## License

Apache 2.0

## Sequence

Sequence file must have no header, contain a single sequence on a single line

## Options

* MaxPatternLen will limit the size of the graph to the maximum size of a match.
* Compressed: uses the compressed graph


## Graph

with generated graph, one can create an image with *dot* tool:

    dot -Tpng graph-compressed.dot -o graph.png

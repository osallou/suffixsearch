module gitlab.inria.fr/osallou/cassiopee2-go/cli

go 1.13

replace gitlab.inria.fr/osallou/suffixsearch => /home/osallou/Development/NOSAVE/gopath/src/gitlab.inria.fr/osallou/suffixsearch

require (
	gitlab.inria.fr/osallou/suffixsearch v0.3.2
	go.uber.org/zap v1.13.0
)

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"

	cassie "gitlab.inria.fr/osallou/suffixsearch"
	"go.uber.org/zap"
)

func isEqual(a byte, b byte) bool {
	if a == b {
		return true
	}
	switch a {
	case 'n':
		return true
	case 'y':
		if b == 'c' || b == 't' {
			return true
		}
	case 'r':
		if b == 'a' || b == 'g' {
			return true
		}
	case 's':
		if b == 'g' || b == 'c' {
			return true
		}
	case 'w':
		if b == 'a' || b == 't' {
			return true
		}
	case 'k':
		if b == 't' || b == 'u' || b == 'g' {
			return true
		}
	case 'm':
		if b == 'a' || b == 'c' {
			return true
		}
	case 'b':
		if b != 'a' {
			return true
		}
	case 'd':
		if b != 'c' {
			return true
		}
	case 'h':
		if b != 'g' {
			return true
		}
	case 'v':
		if b == 'a' || b == 'c' || b == 'g' {
			return true
		}
	}

	switch b {
	case 'n':
		return true
	case 'y':
		if a == 'c' || a == 't' {
			return true
		}
	case 'r':
		if a == 'a' || a == 'g' {
			return true
		}
	case 's':
		if a == 'g' || a == 'c' {
			return true
		}
	case 'w':
		if a == 'a' || a == 't' {
			return true
		}
	case 'k':
		if a == 't' || a == 'u' || a == 'g' {
			return true
		}
	case 'm':
		if a == 'a' || a == 'c' {
			return true
		}
	case 'b':
		if a != 'a' {
			return true
		}
	case 'd':
		if a != 'c' {
			return true
		}
	case 'h':
		if a != 'g' {
			return true
		}
	case 'v':
		if a == 'a' || a == 'c' || a == 'g' {
			return true
		}
	}

	return false
}

func main() {
	logger, _ := zap.NewProduction()
	defer logger.Sync()

	var sequence string
	var pattern string
	var cost int64
	var distance int64
	var compressed bool
	var inmemory bool
	var data string
	var graph string
	flag.StringVar(&pattern, "pattern", "", "search pattern")
	flag.Int64Var(&cost, "cost", 0, "maximum cost")
	flag.Int64Var(&distance, "distance", 0, "maximum distance")
	flag.StringVar(&sequence, "sequence", "", "sequence file path")
	flag.BoolVar(&compressed, "compressed", false, "use compressed tree")
	flag.BoolVar(&inmemory, "memory", false, "use in-memory only (warning: need enough memory, cannot use compressed option)")
	flag.StringVar(&data, "data", "", "string to index instead of sequence file, force in-memory usage")
	flag.StringVar(&graph, "graph", "", "file path to generate graph")
	flag.Parse()

	if pattern == "" {
		fmt.Println("missing pattern")
		os.Exit(1)
	}

	if sequence == "" && data == "" {
		fmt.Println("missing sequence")
		os.Exit(1)
	}

	useData := false
	if data != "" {
		sequence = data
		useData = true
		inmemory = true
	}

	root := cassie.NewRoot(sequence, isEqual, cassie.Options{
		Compressed:    compressed,
		InMemory:      inmemory,
		UseData:       useData,
		MaxPatternLen: 0,
	})
	err := root.Index()
	if err != nil {
		panic(err)
	}
	if graph != "" {
		root.Graph(graph)
	}
	defer root.Close()

	// rootJSON, _ := json.Marshal(root.Edges)
	// fmt.Printf("Root: %s\n", string(rootJSON))

	ch := make(chan cassie.Match)
	go root.Search(ch, pattern, cost, distance)
	matches := make([]cassie.Match, 0)
	for m := range ch {
		m.Len = int64(len(pattern)) + m.Insert - m.Deletion
		matches = append(matches, m)
	}
	resJSON, _ := json.Marshal(matches)
	fmt.Printf("Results: %s\n", string(resJSON))
}

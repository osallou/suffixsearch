package suffixsearch

import (
	"encoding/json"
	"testing"
)

func TestIndexUncompressed(t *testing.T) {
	root := NewRoot("sequence.txt", nil, Options{
		Compressed: false,
	})
	err := root.Index()
	if err != nil {
		t.Errorf("failed to index %+v", err)
	}

	if len(root.Edges) != 4 {
		rootJSON, _ := json.Marshal(root.Edges)
		t.Errorf("should have 4 root edges %s", rootJSON)
	}
	root.generateGraph("graph-uncompressed.dot")
	root.Close()
}

func TestIndexCompressed(t *testing.T) {
	root := NewRoot("sequence.txt", nil, Options{
		Compressed: true,
	})
	err := root.Index()
	if err != nil {
		t.Errorf("failed to index %+v", err)
	}

	if len(root.Edges) != 4 {
		rootJSON, _ := json.Marshal(root.Edges)
		t.Errorf("should have 4 root edges %s", rootJSON)
	}

	root.Close()
}

type Pattern struct {
	Pattern  string
	Expected int
	Errors   int64
	Indel    int64
}

func TestSearchUncompressed(t *testing.T) {

	patterns := []Pattern{
		Pattern{Pattern: "ccc", Expected: 2},
		Pattern{Pattern: "cgt", Expected: 1},
		Pattern{Pattern: "tttttt", Expected: 1},
		Pattern{Pattern: "ttttt", Expected: 2},
		Pattern{Pattern: "tttttg", Expected: 1, Errors: 1},
		Pattern{Pattern: "ccca", Expected: 3, Errors: 1},
		Pattern{Pattern: "acagt", Expected: 1, Indel: 1},
		Pattern{Pattern: "ccccaaaaaa", Expected: 1},
	}

	for _, pattern := range patterns {
		root := NewRoot("sequence.txt", isEqualExact, Options{
			Compressed:    false,
			MaxPatternLen: int64(len(pattern.Pattern)),
		})
		err := root.Index()
		if err != nil {
			t.Errorf("failed to index %+v", err)
		}

		ch := make(chan Match)
		go root.Search(ch, pattern.Pattern, pattern.Errors, pattern.Indel)
		matches := make([]Match, 0)
		for m := range ch {
			m.Len = int64(len(pattern.Pattern)) + m.Insert - m.Deletion
			matches = append(matches, m)
		}
		if len(matches) != pattern.Expected {
			t.Errorf("should have %d matches %d", pattern.Expected, len(matches))
		}
	}
}

func TestSearchCompressed(t *testing.T) {

	patterns := []Pattern{
		Pattern{Pattern: "ccc", Expected: 2},
		Pattern{Pattern: "cgt", Expected: 1},
		Pattern{Pattern: "tttttt", Expected: 1},
		Pattern{Pattern: "ttttt", Expected: 2},
		Pattern{Pattern: "tttttg", Expected: 1, Errors: 1},
		Pattern{Pattern: "ccca", Expected: 3, Errors: 1},
		Pattern{Pattern: "acagt", Expected: 1, Indel: 1},
		Pattern{Pattern: "ccccaaaaaa", Expected: 1},
	}

	for _, pattern := range patterns {
		root := NewRoot("sequence.txt", isEqualExact, Options{
			Compressed:    true,
			MaxPatternLen: int64(len(pattern.Pattern)),
		})
		err := root.Index()
		if err != nil {
			t.Errorf("failed to index %+v", err)
		}

		ch := make(chan Match)
		go root.Search(ch, pattern.Pattern, pattern.Errors, pattern.Indel)
		matches := make([]Match, 0)
		for m := range ch {
			m.Len = int64(len(pattern.Pattern)) + m.Insert - m.Deletion
			matches = append(matches, m)
		}
		if len(matches) != pattern.Expected {
			t.Errorf("%s should have %d matches %d", pattern.Pattern, pattern.Expected, len(matches))
		}
	}
}

func TestGenerateGraph(t *testing.T) {
	root := NewRoot("sequence.txt", nil, Options{
		Compressed: true,
	})
	err := root.Index()
	if err != nil {
		t.Errorf("failed to index %+v", err)
	}
	root.generateGraph("graph-compressed.dot")
	root.Close()
}

func TestGenerateSigmaGraph(t *testing.T) {
	root := NewRoot("sequence.txt", nil, Options{
		Compressed: true,
	})
	err := root.Index()
	if err != nil {
		t.Errorf("failed to index %+v", err)
	}
	defer root.Close()

	sigmaJSON := root.generateSigmaGraph()
	if sigmaJSON == "" {
		t.Errorf("Failed to generate sigma graph")
	}

}

package suffixsearch

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"go.uber.org/zap"
)

// Count is a counter for unique node ids
var count int

// Match represent a solution in search
type Match struct {
	Position int64
	Subst    int64
	Insert   int64
	Deletion int64
	Len      int64
}

// Options contains some index and search options
type Options struct {
	MaxPatternLen int64
	Compressed    bool
	InMemory      bool
	UseData       bool
}

// Root is root of the graph
type Root struct {
	sequence     string
	sequencePath string
	seqFile      *os.File
	seqSize      int64
	Edges        []*Node
	options      Options
	logger       *zap.Logger
	fn           equalfn
}

// Position info
type Position struct {
	Position int64
	Len      int64 // Remaining length
}

// Node is a node in graph
type Node struct {
	ID        int64
	Level     int64
	Value     byte
	Positions []Position
	Edges     []*Node
	nextPos   int64 // Next char position
	nextLen   int64 // Length of remaining data to read
}

type equalfn func(byte, byte) bool

func isEqualExact(a byte, b byte) bool {
	if a == b {
		return true
	}
	return false
}

// IsEqual TODO
func (n *Node) IsEqual(b byte, fn equalfn) bool {
	a := n.Value
	return fn(a, b)
}

func (n *Node) hasEdge(suffix byte) (bool, *Node) {
	if n == nil {
		return false, nil
	}
	for _, edge := range n.Edges {
		if edge.Value == suffix {
			return true, edge
		}
	}
	return false, nil
}

func (n *Node) leafPositions() []Position {
	//fmt.Printf("OSALLOU ?? %+v\n", n)
	positions := n.Positions
	for _, edge := range n.Edges {
		subPositions := edge.leafPositions()
		positions = append(positions, subPositions...)
	}
	return positions
}

// NewRoot creates a new graph for input sequence path
// fn is a function of type equalfn to compare characters, if nil,
// it makes an exact comparison.
func NewRoot(sequence string, fn equalfn, options Options) *Root {
	root := Root{
		sequencePath: sequence,
		options:      options,
		fn:           fn,
	}
	if options.UseData {
		root.sequence = sequence
		root.sequencePath = ""
		root.options.Compressed = false
	}

	if fn == nil {
		fn = isEqualExact
	}
	root.Edges = make([]*Node, 0)

	if os.Getenv("DEBUG") == "1" {
		root.logger, _ = zap.NewDevelopment()
	} else {
		root.logger, _ = zap.NewProduction()
	}

	return &root
}

// Search search for a string with substitution and/or insert/deletion
// Results are sent back in input chan
func (r *Root) Search(ch chan Match, suffix string, cost int64, indel int64) error {
	for _, node := range r.Edges {
		r.search(ch, node, suffix, 0, 0, 0, cost, indel, "", "")
	}
	close(ch)
	return nil
}

func (r *Root) searchExtraCompressed(ch chan Match, pos Position, extraSuffix string, suffix string, cost int64, in int64, del int64, maxcost int64, maxdist int64, data1 string, data2 string) {
	r.logger.Debug("searchExtraCompressed", zap.Any("position", pos), zap.String("extra", extraSuffix), zap.String("suffix", suffix))
	doMatch := true
	newdata1 := data1
	newdata2 := data2
	//newcost := cost
	for i := 0; i < len(suffix); i++ {
		if i >= len(extraSuffix) {
			var j int64
			tmpdata1 := data1
			tmpdata2 := data2

			for j = 0; j < (int64(len(suffix)) - int64(i)); j++ {

				if in+del < maxdist {
					tmpdata1 += "-"
					tmpdata2 += " "
					r.logger.Debug("Match", zap.String("data1", newdata1), zap.String("data2", newdata2))
					ch <- Match{
						Position: pos.Position,
						Subst:    cost,
						Insert:   in,
						Deletion: del + j,
						Len:      pos.Len,
					}
				} else {
					break
				}

			}
			doMatch = false
			break
		}
		if !r.fn(suffix[i], extraSuffix[i]) {
			if cost < maxcost {
				newdata1 += "X"
				newdata2 += string(extraSuffix[i])
				//newcost++
				cost++
			} else {
				doMatch = false
				//break
			}
			if in+del < maxdist {
				// allow insert and del
				tmpdata1 := data1 + "-"
				tmpdata2 := data2 + " "
				r.logger.Debug("Try del ", zap.String("extra", extraSuffix), zap.String("suffix", suffix[1:]))
				r.searchExtraCompressed(ch, pos, extraSuffix, suffix[1:], cost, in, del+1, maxcost, maxdist, tmpdata1, tmpdata2)
				tmpdata1 = data1 + "+"
				tmpdata2 = data2 + string(extraSuffix[i])
				r.logger.Debug("Try in ", zap.String("extra", extraSuffix[1:]), zap.String("suffix", suffix))
				r.searchExtraCompressed(ch, pos, extraSuffix[1:], suffix, cost, in+1, del, maxcost, maxdist, tmpdata1, tmpdata2)
				break
			}
		} else {
			newdata1 += string(suffix[i])
			newdata2 += string(extraSuffix[i])
		}
		if in+del < maxdist {
			// allow insert and del
			tmpdata1 := data1 + "-"
			tmpdata2 := data2 + " "
			r.logger.Debug("Try del ", zap.String("extra", extraSuffix), zap.String("suffix", suffix[1:]))
			r.searchExtraCompressed(ch, pos, extraSuffix, suffix[1:], cost, in, del+1, maxcost, maxdist, tmpdata1, tmpdata2)
			tmpdata1 = data1 + "+"
			tmpdata2 = data2 + string(extraSuffix[i])
			r.logger.Debug("Try in ", zap.String("extra", extraSuffix[1:]), zap.String("suffix", suffix))
			r.searchExtraCompressed(ch, pos, extraSuffix[1:], suffix, cost, in+1, del, maxcost, maxdist, tmpdata1, tmpdata2)
		}
	}
	if doMatch {
		r.logger.Debug("Match", zap.String("data1", newdata1), zap.String("data2", newdata2))
		ch <- Match{
			Position: pos.Position,
			//Subst:    newcost,
			Subst:    cost,
			Insert:   in,
			Deletion: del,
			Len:      pos.Len,
		}
	}
}

func (r *Root) searchCompressed(ch chan Match, node *Node, suffix string, cost int64, in int64, del int64, maxcost int64, maxdist int64, data1 string, data2 string) {
	r.logger.Debug("searchCompressed", zap.Any("node", node), zap.Int("nbpos", len(node.Positions)), zap.Int64("level", node.Level))
	r.logger.Debug("data", zap.String("data1", reduceSuffix(data1)), zap.String("data2", reduceSuffix(data2)))
	for _, pos := range node.Positions {
		if pos.Len <= 0 {
			continue
		}
		extraSuffix, errSuffix := r.readSuffix(pos.Position+node.Level+1, int64(len(suffix))+maxdist)
		if errSuffix != nil {
			r.logger.Error("Failed to read extra suffix in sequence", zap.Int64("position", pos.Position), zap.Int64("len", pos.Len))
			continue
		}
		r.searchExtraCompressed(ch, pos, extraSuffix, suffix, cost, in, del, maxcost, maxdist, data1, data2)

	}
}

func (r *Root) search(ch chan Match, node *Node, suffix string, cost int64, in int64, del int64, maxcost int64, maxdist int64, data1 string, data2 string) {

	if len(suffix) == 0 {
		newdata1 := data1 + "+"
		newdata2 := data2 + string(node.Value)
		if in+del < maxdist {
			for _, pos := range node.leafPositions() {
				var i int64
				for i = 0; i < (maxdist - (in + del)); i++ {
					if int64(len(suffix))+i > pos.Len {
						break
					}
					r.logger.Debug("Match", zap.Int64("pos", pos.Position), zap.String("data1", newdata1), zap.String("data2", newdata2))
					ch <- Match{
						Position: pos.Position,
						Subst:    cost,
						Insert:   in + i,
						Deletion: del,
						Len:      pos.Len,
					}
				}
			}
			for _, edge := range node.Edges {
				r.search(ch, edge, suffix, cost+1, in+1, del, maxcost, maxdist, newdata1, newdata2)
			}
		}
		return
	}
	if node.IsEqual(suffix[0], r.fn) {
		newdata1 := data1 + string(suffix[0])
		newdata2 := data2 + string(node.Value)
		if len(suffix) == 1 {
			// End of suffix, find all positions now
			positions := make([]int64, 0)
			for _, pos := range node.leafPositions() {
				found := false
				// avoid duplicates
				r.logger.Debug("leaf", zap.Int64("pos", pos.Position))
				for _, p := range positions {
					if p == pos.Position {
						found = true
						break
					}
				}

				if found {
					continue
				}
				positions = append(positions, pos.Position)
				r.logger.Debug("Match", zap.Int64("pos", pos.Position), zap.String("data1", newdata1), zap.String("data2", newdata2))
				ch <- Match{
					Position: pos.Position,
					Subst:    cost,
					Insert:   in,
					Deletion: del,
					Len:      pos.Len,
				}
			}
			return
		}

		for _, edge := range node.Edges {
			// Continue the search
			r.search(ch, edge, suffix[1:], cost, in, del, maxcost, maxdist, newdata1, newdata2)
		}
		if r.options.Compressed && len(node.Edges) == 0 {
			r.searchCompressed(ch, node, suffix[1:], cost, in, del, maxcost, maxdist, newdata1, newdata2)
		}
	} else {
		if cost < maxcost {

			newdata1 := data1 + "X"
			newdata2 := data2 + string(node.Value)

			if len(suffix) == 1 {
				// End of suffix, find all positions now
				positions := make([]int64, 0)
				// avoid duplicates
				for _, pos := range node.leafPositions() {
					r.logger.Debug("leaf", zap.Int64("pos", pos.Position))
					found := false

					for _, p := range positions {
						if p == pos.Position {
							found = true
							break
						}
					}

					if found {
						continue
					}
					positions = append(positions, pos.Position)
					r.logger.Debug("Match", zap.Int64("pos", pos.Position), zap.String("data1", newdata1), zap.String("data2", newdata2))
					ch <- Match{
						Position: pos.Position,
						Subst:    cost + 1,
						Insert:   in,
						Deletion: del,
						Len:      pos.Len,
					}
				}
				return
			}

			// go to next and increase cost
			for _, edge := range node.Edges {
				// Continue the search
				r.search(ch, edge, suffix[1:], cost+1, in, del, maxcost, maxdist, newdata1, newdata2)
			}
			if r.options.Compressed && len(node.Edges) == 0 {
				r.searchCompressed(ch, node, suffix[1:], cost+1, in, del, maxcost, maxdist, newdata1, newdata2)
			}

		}
	}
	if in+del < maxdist {
		// go to next with in and del
		newdata1 := data1 + "-"
		newdata2 := data2 + " "
		r.logger.Debug("try del", zap.String("data1", newdata1), zap.String("data2", newdata2))
		r.search(ch, node, suffix[1:], cost, in, del+1, maxcost, maxdist, newdata1, newdata2)
		for _, edge := range node.Edges {
			newdata1 := data1 + "+"
			newdata2 := data2 + string(node.Value)
			r.logger.Debug("try in", zap.String("data1", newdata1), zap.String("data2", newdata2))
			r.search(ch, edge, suffix, cost, in+1, del, maxcost, maxdist, newdata1, newdata2)
		}
		if r.options.Compressed && len(node.Edges) == 0 {
			r.searchCompressed(ch, node, suffix[:1], cost, in+1, del, maxcost, maxdist, newdata1, newdata2)
		}
		if len(node.Edges) == 0 && !r.options.Compressed {
			var i int64
			for i = 0; i < (maxdist - (in + del)); i++ {
				for _, pos := range node.Positions {
					r.logger.Debug("Match", zap.String("data1", newdata1), zap.String("data2", newdata2))
					ch <- Match{
						Position: pos.Position,
						Subst:    cost + 1,
						Insert:   in,
						Deletion: del + i,
						Len:      pos.Len,
					}
				}
			}
		}
	}
}

// Index creates the index in memory
func (r *Root) Index() error {
	var err error
	count = 0

	if r.options.UseData {
		r.logger.Debug("Use input sequence")
		r.seqSize = int64(len(r.sequence))

	} else {
		r.logger.Debug("Index sequence", zap.String("sequence", r.sequencePath))
		r.seqFile, err = os.Open(r.sequencePath)
		if err != nil {
			r.logger.Error("failed to open sequence file", zap.String("sequence", r.sequencePath))
			return err
		}
		stat, _ := r.seqFile.Stat()
		r.seqSize = stat.Size()
		if r.options.InMemory {
			seqBytes, _ := ioutil.ReadFile(r.sequencePath)
			r.sequence = string(seqBytes)
		}
	}

	if r.options.MaxPatternLen == 0 {
		r.logger.Debug("use seqsize pattern len", zap.Int64("size", r.seqSize))
		r.options.MaxPatternLen = r.seqSize
	}

	var i int64
	for i = 0; i < r.seqSize; i++ {
		suffix, err := r.readSuffix(i, r.options.MaxPatternLen)
		if err != nil {
			return err
		}
		r.fillTree(i, suffix, nil, 0)
	}
	return err
}

// Close close and delete resources
func (r *Root) Close() {
	r.logger.Sync()
	r.seqFile.Close()
}

func reduceSuffix(suffix string) string {
	if len(suffix) > 10 {
		return suffix[0:10] + "..."
	}
	return suffix
}

func (r *Root) fillTree(position int64, suffix string, node *Node, level int64) error {
	r.logger.Debug("fillTree", zap.Int64("level", level), zap.Int64("position", position), zap.String("suffix", reduceSuffix(suffix)))
	if len(suffix) == 0 || suffix == "\n" {
		return nil
	}
	var edges []*Node
	if node == nil {
		edges = r.Edges
	} else {
		edges = node.Edges
	}
	c := suffix[0]
	//for _, c := range suffix {
	found := false
	var edge *Node
	for _, e := range edges {
		//r.logger.Debug("compare if exists", zap.String("suffix", string(c)), zap.String("edge", string(e.value)))
		if c == e.Value {
			found = true
			edge = e
			break
		}
	}

	// In case of compression and no edge is present, need to compare what's after
	if found {
		r.logger.Debug("has edges?", zap.String("suffix", suffix), zap.Bool("compressed", r.options.Compressed), zap.Int("edges", len(edge.Edges)))
	}
	//if r.options.Compressed && len(edges) == 0 && node != nil {
	if r.options.Compressed && found && len(edge.Edges) == 0 {
		//curNode := node
		remainingPositions := make([]Position, 0)
		r.logger.Debug("check for existing positions", zap.Int("nbpos", len(edge.Positions)))
		for index, pos := range edge.Positions {
			curNode := edge
			if pos.Len == 0 {
				remainingPositions = append(remainingPositions, pos)
				continue
			}
			i := 0
			tmpLevel := level
			//isEqual := true
			after, _ := r.readSuffix(pos.Position, int64(len(suffix)))
			r.logger.Debug("compare remaining", zap.Int64("pos", pos.Position), zap.String("suffix", suffix), zap.String("after", after))

			if strings.HasSuffix(after, suffix) {
				edge.Positions = append(edge.Positions, Position{
					Position: position,
					Len:      int64(len(suffix) - 1),
				})
				return nil

			}
			for i < len(suffix) && i < len(after) {
				if i == 0 {
					i++
					continue
				}
				if after[i] == suffix[i] {
					r.logger.Debug("after matches for pos", zap.Int("index", index), zap.Int64("pos", pos.Position), zap.Int("i", i))
					edgeCreated, newedge := curNode.hasEdge(after[i])
					if edgeCreated {
						curNode = newedge
						tmpLevel++
					} else {
						extendNode := Node{
							ID:        int64(count),
							Value:     after[i],
							Edges:     make([]*Node, 0),
							Positions: make([]Position, 0),
							Level:     tmpLevel + 1,
						}
						count++
						curNode.Edges = append(curNode.Edges, &extendNode)
						curNode = &extendNode
						tmpLevel++
					}
				} else {
					r.logger.Debug("not equal anymore, need to break here", zap.Int64("pos", pos.Position), zap.Int("suffix index", i))
					edgeCreated, newedge := curNode.hasEdge(after[i])
					if edgeCreated {
						newedge.Positions = append(newedge.Positions, pos)

					} else {
						r.logger.Debug("extend node and add", zap.String("current", string(curNode.Value)), zap.String("after", string(after[i])))
						extendNode := Node{
							ID:        int64(count),
							Value:     after[i],
							Edges:     make([]*Node, 0),
							Positions: make([]Position, 1),
							Level:     tmpLevel + 1,
						}
						count++
						extendNode.Positions[0] = pos
						curNode.Edges = append(curNode.Edges, &extendNode)

						tmpLevel++
					}
					break
				}
				i++
			}
		}
		edgeCreated, newedge := node.hasEdge(suffix[0])
		if edgeCreated {
			found = true
			edge = newedge
		}
		edge.Positions = remainingPositions
	}

	if found {
		// r.logger.Debug("should go to", zap.String("newsuffix", suffix[1:]), zap.String("suffix", suffix), zap.String("edge", string(edge.Value)))
		edge.Positions = append(edge.Positions, Position{
			Position: position,
			Len:      int64(len(suffix[1:])),
		})
		r.fillTree(position, suffix[1:], edge, level+1)
	} else {
		positions := make([]Position, 1)
		positions[0] = Position{
			Position: position,
			Len:      int64(len(suffix[1:])),
		}
		newNode := Node{
			ID:        int64(count),
			Value:     c,
			Positions: positions,
			Edges:     make([]*Node, 0),
			Level:     level,
		}
		count++
		r.logger.Debug("new node", zap.String("value", string(c)), zap.Any("positions", newNode.Positions))

		if node == nil {
			r.Edges = append(r.Edges, &newNode)

			edges = r.Edges
		} else {
			node.Edges = append(node.Edges, &newNode)
			edges = node.Edges
		}

		if !r.options.Compressed {
			r.fillTree(position, suffix[1:], &newNode, level+1)
		}
	}
	//}
	return nil
}

// readSuffix tries to read *len* chars from sequence from *position*
func (r *Root) readSuffix(pos int64, length int64) (string, error) {
	bufferSize := length
	if pos+length > r.seqSize {
		//r.logger.Debug("try to read more than sequence size", zap.Int64("pos", pos), zap.Int64("len", len), zap.Int64("size", r.seqSize))
		bufferSize = r.seqSize - pos
	}
	if r.options.InMemory {
		return r.sequence[pos : pos+bufferSize], nil
	}

	buffer := make([]byte, bufferSize)
	_, err := r.seqFile.ReadAt(buffer, pos)
	if err != nil {
		r.logger.Error("Failed to read from sequence", zap.Error(err))
		return "", err
	}
	// get content
	content := string(buffer)
	if len(content) > 0 && string(content[len(content)-1]) == "\n" {
		return content[0 : len(content)-1], nil
	}
	return content, nil
}

type edgeElt struct {
	ID     string `json:"id"`
	Source string `json:"source"`
	Target string `json:"target"`
}
type nodeElt struct {
	ID    string `json:"id"`
	Label string `json:"label"`
	X     uint   `json:"x"`
	Y     uint   `json:"y"`
	Size  uint   `json:"size"`
}
type sigmaGraph struct {
	Nodes []nodeElt `json:"nodes"`
	Edges []edgeElt `json:"edges"`
}

func (s *sigmaGraph) generate(node *Node) {
	s.Nodes = append(s.Nodes, nodeElt{
		ID:    strconv.Itoa(int(node.ID)),
		Label: string(node.Value),
	})
	for _, edge := range node.Edges {
		s.Edges = append(s.Edges, edgeElt{
			ID:     strconv.Itoa(count),
			Source: strconv.Itoa(int(node.ID)),
			Target: strconv.Itoa(int(edge.ID)),
		})
		count++
		s.generate(edge)
	}
}

func (r *Root) generateSigmaGraph() string {
	sigma := &sigmaGraph{
		Nodes: make([]nodeElt, 0),
		Edges: make([]edgeElt, 0),
	}
	sigma.Nodes = append(sigma.Nodes, nodeElt{
		ID:    "root",
		Label: "root",
	})
	count = 0
	for _, node := range r.Edges {
		sigma.Edges = append(sigma.Edges, edgeElt{
			ID:     strconv.Itoa(count),
			Source: "root",
			Target: strconv.Itoa(int(node.ID)),
		})
		count++
		sigma.generate(node)
	}
	sigmaJSON, _ := json.Marshal(*sigma)

	return string(sigmaJSON)
}

// Graph generates sigma graph
func (r *Root) Graph(outfile string) {
	r.generateGraph(outfile)
}

func (r *Root) generateGraph(outfile string) {
	graph := "digraph cassie {\n"
	for _, node := range r.Edges {
		graph += fmt.Sprintf("root -> %s%d_%d_%d;\n", string(node.Value), node.ID, node.Level, len(node.leafPositions()))
		graph += node.generateGraph()
	}
	graph += "}"
	ioutil.WriteFile(outfile, []byte(graph), 0644)

}

func (n *Node) generateGraph() string {
	graph := ""
	for _, edge := range n.Edges {
		graph += fmt.Sprintf("%s%d_%d_%d -> %s%d_%d_%d;\n", string(n.Value), n.ID, n.Level, len(n.leafPositions()), string(edge.Value), edge.ID, edge.Level, len(edge.leafPositions()))
		graph += edge.generateGraph()

	}
	return graph
}
